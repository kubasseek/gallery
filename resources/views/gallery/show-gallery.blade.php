@extends('master')



@section('content')
    <style>
        #gallery-images img{
            width:240px;
            height:160px;
            margin-bottom: 10px;
        }
        #gallery-images ul{
            margin: 0px;
            padding: 0px;
        }
        #gallery-images li{
            margin: 0px;
            padding: 0px;
            float: left;
            list-style: none;
            padding-right: 10px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <h1>{{$gallery->name}}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div id="gallery-images">
                <ul>
                    @foreach($gallery->images as $image)
                        <li>
                            <a href="{{ url($image->file_path) }}" target="_blank" data-lightbox="{{ $gallery->id }}">
                                <img src="{{ url('gallery/images/thumbnails/' . $image->file_name) }}">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-10">
            <form action="{{ url('upload') }}"
            class="dropzone" id="addImages"
            >
                {{ csrf_field() }}
                <input type="hidden" name="gallery_id" value="{{ $gallery->id }}">
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('gallery/list') }}" class="btn btn-primary">Cofnij do listy</a>
        </div>
    </div>
@endsection