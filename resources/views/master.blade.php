<!DOCTYPE html>
<html>

<head>

    <title>
        tytuł
    </title>
    <!-- Material Design fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Material Design -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.8/css/bootstrap-material-design.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.8/css/ripples.css" rel="stylesheet">

    <!-- Dropdown.js -->
    <link href="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">

    <!-- Page style -->
    <link href="index.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/lightbox.css') }}">

    <script>
        var baseUrl = "{{ url('/') }}"

        @yield('js')
    </script>
    <style>
        @yield('css')
    </style>
</head>

<body>

    <div class="container">
        @if(Session::has('Error'))
            <div class="alert alert-danger">{{ Session::get('Error') }}</div>
        @endif

        @if(Session::has('Success'))
            <div class="alert alert-success">{{ Session::get('Success') }}</div>
        @endif
    </div>

    <div class="container">

        @yield('content')

    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>

    <script type="text/javascript" src="{{ url(elixir('js/all.js')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/lightbox.min.js') }}"></script>

    <!-- Twitter Bootstrap -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <!-- Material Design for Bootstrap -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.8/js/material.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.8/js/ripples.js"></script>
    <script>
        $.material.init();
    </script>

    <!-- Dropdown.js -->
    <script src="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.js"></script>
    <script>
        $("#dropdown-menu select").dropdown();
    </script>
</body>

</html>