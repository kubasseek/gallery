@extends('master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1>Moje albumy</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">
            @if ($galleries->count() > 0)
                <table class="table table-striped table-bordered table-responsive">
                    <thead>
                        <tr class="info">
                            <td style="width: 70%">Nazwa albumu</td>
                            <td>Akcje</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($galleries as $gallery)
                            <tr>
                                <td>{{ $gallery->name }}
                                    <span class="pull-right">
                                        {{ $gallery->images()->count() }}
                                    </span>
                                </td>
                                <td><a href="{{ url('gallery/view/' . $gallery->id) }}">Zobacz album</a> {{ ' / ' }} <a href="{{ url('gallery/delete/' . $gallery->id) }}">Usuń</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <h3>Nic tutaj nie ma :(
                    <br>
                        Po prawej stronie możesz dodać swoją pierwszą galerię.</h3>
                @endif
        </div>

        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="/logout">Wyloguj</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </div>


        <div class="col-md-3">
            @if(count($errors) > 0)
                <div class="alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form" method="POST" action="{{ url('gallery/save') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <input type="text" name="gallery_name"
                            id="gallery_name" placeholder="Wpisz nazwę abumu"
                    class="form-control"
                    value="{{ old('gallery_name') }}"/>
                </div>
                <button class="btn btn-primary">Dodaj</button>

            </form>
        </div>
    </div>

@endsection