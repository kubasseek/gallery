<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => '1stUser',
            'email'     => '1@1.com',
            'password'  => Hash::make('pass'),
        ]);
        User::create([
            'name'      => '2ndUser',
            'email'     => '2@2.com',
            'password'  => Hash::make('pass'),
        ]);
        User::create([
            'name'      => '3rdUser',
            'email'     => '3@3.com',
            'password'  => Hash::make('pass'),
        ]);
    }
}
